#!/bin/sh
# NOTE: The test assumes that we have a ':' entry on the J2F_DIRLIST variable
DATE="$(date)"
test_sock() {
    sudo curl --header "Content-Type: application/json" \
              --request POST \
              --data "{\"date\":\"$DATE\",\"type\":\"socket\"}" \
              --unix-socket "/run/json2file-go/socket" \
              http://localhost/
}
test_http() {
    sudo curl --header "Content-Type: application/json" \
              --request POST \
              --data "{\"date\":\"$DATE\",\"test\":\"http\"}" \
              http://localhost/json/
}
case "$1" in
http) test_http;;
sock) test_sock;;
*) echo "Usage: $0 {http|sock}";;
esac
