#!/bin/sh
# NOTE: The test assumes that we have a ':' entry on the J2F_DIRLIST variable
DATE="$(date)"
test_https() {
    sudo curl --header "Content-Type: application/json" \
              --request POST \
              --data "{\"date\":\"$DATE\",\"type\":\"socket\"}" \
              --insecure \
              https://localhost/
}
case "$1" in
https) test_https;;
*) echo "Usage: $0 {https}";;
esac
