# VERSION:
#   VERSION="$(sed -e 's/^.*(\(.*\)).*$/\1/; q' debian/changelog)"
# BUILD:
#   docker build . -t json2file-go:$VERSION
# RUN:
#   docker run -v $(pwd)/data:/data --rm --name json2file-go    \
#              -e JSON2FILE_DIRLIST="test1:123456;test2:" json2file-go:$VERSION
FROM golang:1.14-alpine as builder
WORKDIR /src
RUN apk update && apk add --no-cache git && rm -rf /var/cache/apk/*
RUN go get github.com/julienschmidt/httprouter
COPY json2file.go .
RUN CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -ldflags "-s -w" \
     -o json2file json2file.go

FROM scratch
WORKDIR /
COPY --from=builder /src/json2file .
ENV J2F_BASEDIR /data
VOLUME ["/data"]
CMD ["./json2file"]
